class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :owner
      t.string :name
      t.integer :price
      t.integer :number
      t.boolean :is_paid
      t.integer :order_id
      t.integer :restaurant_id

      t.timestamps null: false
    end
  end
end
