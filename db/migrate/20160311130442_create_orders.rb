class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.text :description
      t.integer :restaurant_id
      t.datetime :order_at
      t.string :aasm_state
      t.string :handler

      t.timestamps null: false
    end
  end
end
