class PopulationProcessController < ApplicationController
  def index

  end

  def root
    begin
      if !params[:population]
        flash[:error] = "抱歉，您沒有選擇檔案"
        redirect_to population_process_index_path
        return
      end

      upload_file = params[:population][:source_file]
      row_length = params[:population][:row_length]
      row_length = row_length ? row_length.to_i : nil

      hash = Digest::SHA256.hexdigest Time.now.to_s
      source_path = "tmp/#{hash}.csv"
      result_path = "tmp/#{hash}-result.csv"

      File.open(source_path, 'wb') do |f|
        f.write(upload_file.read)
      end

      c = CountryPopulationCalculator.new

      if c.run(source_path, result_path, row_length)
        send_file(result_path, filename: "結果-#{upload_file.original_filename}")
      else
        flash[:error] = "抱歉，處理時發生錯誤: 本檔案無法在有限時間找出解答"
        redirect_to population_process_index_path
      end
    rescue
      flash[:error] = "抱歉，處理時發生錯誤"
      redirect_to population_process_index_path
    end
  end

  def root_couple
    begin
      if !params[:population] || !params[:population][:source_file_male] || !params[:population][:source_file_female]
        flash[:error] = "抱歉，您沒有選擇檔案"
        redirect_to population_process_index_path
        return
      end

      upload_file_male = params[:population][:source_file_male]
      upload_file_female = params[:population][:source_file_female]

      row_length = params[:population][:row_length]
      row_length = row_length ? row_length.to_i : nil

      hash = Digest::SHA256.hexdigest Time.now.to_s

      source_male_path = "tmp/#{hash}-m.csv"
      source_female_path = "tmp/#{hash}-f.csv"

      result_male_path = "tmp/#{hash}-m-result.csv"
      result_female_path = "tmp/#{hash}-f-result.csv"
      result_zip_path  = "tmp/#{hash}-result.csv"

      File.open(source_male_path, 'wb') do |f|
        f.write(upload_file_male.read)
      end

      File.open(source_female_path, 'wb') do |f|
        f.write(upload_file_female.read)
      end

      c = CountryPopulationGenderBalanceCalculator.new

      if c.run(source_male_path, source_female_path, result_male_path, result_female_path, row_length)
        Zip::File.open(result_zip_path, Zip::File::CREATE) do |zipfile|
          zipfile.add("male.csv", result_male_path)
          zipfile.add("female.csv", result_female_path)
        end

        send_file(result_zip_path, filename: "結果.zip")
      else
        flash[:error] = "抱歉，處理時發生錯誤: 本檔案無法在有限時間找出解答"
        redirect_to population_process_index_path
      end
    rescue
      flash[:error] = "抱歉，處理時發生錯誤"
      redirect_to population_process_index_path
    end
  end

  def age_range
    begin
      if !params[:population] || !params[:population][:source_all_file] || !params[:population][:source_file_age_range]
        flash[:error] = "抱歉，您沒有選擇檔案"
        redirect_to population_process_index_path
        return
      end

      upload_file_all = params[:population][:source_all_file]
      upload_file_age_range = params[:population][:source_file_age_range]

      hash = Digest::SHA256.hexdigest Time.now.to_s

      source_all_path = "tmp/#{hash}-all.csv"
      source_age_range_path = "tmp/#{hash}-age-range.csv"

      result_path = "tmp/#{hash}-result.csv"

      File.open(source_all_path, 'wb') do |f|
        f.write(upload_file_all.read)
      end

      File.open(source_age_range_path, 'wb') do |f|
        f.write(upload_file_age_range.read)
      end

      c = CountryPopulationExpandAgeCalculator.new

      if c.run(source_age_range_path, source_all_path, result_path)
        send_file(result_path, filename: "結果-#{upload_file_age_range.original_filename}")
      else
        flash[:error] = "抱歉，處理時發生錯誤: 本檔案無法在有限時間找出解答"
        redirect_to population_process_index_path
      end
    rescue
      flash[:error] = "抱歉，處理時發生錯誤"
      redirect_to population_process_index_path
    end
  end

  def general
    begin
      if !params[:population]
        flash[:error] = "抱歉，您沒有選擇檔案"
        redirect_to population_process_index_path
        return
      end

      upload_file = params[:population][:source_file]
      row_length = params[:population][:row_length]
      row_length = row_length ? row_length.to_i : nil

      hash = Digest::SHA256.hexdigest Time.now.to_s
      source_path = "tmp/#{hash}.csv"
      result_path = "tmp/#{hash}-result.csv"

      File.open(source_path, 'wb') do |f|
        f.write(upload_file.read)
      end

      c = GeneralPopulationCalculator.new

      if c.run(source_path, result_path, row_length)
        send_file(result_path, filename: "結果-#{upload_file.original_filename}")
      else
        flash[:error] = "抱歉，處理時發生錯誤: 本檔案無法在有限時間找出解答"
        redirect_to population_process_index_path
      end
    rescue
      flash[:error] = "抱歉，處理時發生錯誤"
      redirect_to population_process_index_path
    end
  end

  def general_couple
    begin
      if !params[:population] || !params[:population][:source_file_male] || !params[:population][:source_file_female]
        flash[:error] = "抱歉，您沒有選擇檔案"
        redirect_to population_process_index_path
        return
      end

      upload_file_male = params[:population][:source_file_male]
      upload_file_female = params[:population][:source_file_female]

      row_length = params[:population][:row_length]
      row_length = row_length ? row_length.to_i : nil

      hash = Digest::SHA256.hexdigest Time.now.to_s

      source_male_path = "tmp/#{hash}-m.csv"
      source_female_path = "tmp/#{hash}-f.csv"

      result_male_path = "tmp/#{hash}-m-result.csv"
      result_female_path = "tmp/#{hash}-f-result.csv"
      result_zip_path  = "tmp/#{hash}-result.csv"

      File.open(source_male_path, 'wb') do |f|
        f.write(upload_file_male.read)
      end

      File.open(source_female_path, 'wb') do |f|
        f.write(upload_file_female.read)
      end

      c = GeneralPopulationGenderBalanceCalculator.new

      if c.run(source_male_path, source_female_path, result_male_path, result_female_path, row_length)
        Zip::File.open(result_zip_path, Zip::File::CREATE) do |zipfile|
          zipfile.add("male.csv", result_male_path)
          zipfile.add("female.csv", result_female_path)
        end

        send_file(result_zip_path, filename: "結果.zip")
      else
        flash[:error] = "抱歉，處理時發生錯誤: 本檔案無法在有限時間找出解答"
        redirect_to population_process_index_path
      end
    rescue
      flash[:error] = "抱歉，處理時發生錯誤"
      redirect_to population_process_index_path
    end
  end
end
