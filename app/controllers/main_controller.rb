class MainController < ApplicationController
  before_filter :authenticate_user!, only: [:add_item, :delete_item]

  def index
    @user = current_user
    @order = current_order
    @active_orders = Order.active

    if @user.nil?
      render :welcome
      return
    end
  end

  def add_item
    item = Item.new(item_params)

    if item.save
      order = item.order

      render json: {
        success: true,
        items: order.items
      }
    end
  end

  def delete_item
    item = Item.find(params[:item_id])
    order = item.order

    if item.owner == current_user['name'] and item.destroy
      render json: {
        success: true,
        items: order.items
      }
    end
  end

  def typehead
    # keyword
    # restaurant_id
  end

  private

    def current_order
      order = nil

      if !params[:order_id].nil?
        begin
          order = Order.find(params[:order_id])
          order = nil if !order.active?
        rescue
          # order_id not found
          params[:order_id] = nil
        end
      end

      if params[:order_id].nil?
        order = Order.active.first
      end

      order
    end

    def item_params
      params.require(:item)
        .permit(:owner, :name, :price,
          :order_id, :restaurant_id)
    end
end
