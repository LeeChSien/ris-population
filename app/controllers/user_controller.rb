class UserController < ApplicationController
  # User:
  # => name
  # =>
  #

  def login
    session[:user] = {
        'name' => params[:user][:name]
      }
    session[:expires_at] = Time.current + 1.year

    redirect_to root_url
  end

  def logout
    session[:user] = nil

    redirect_to root_url
  end
end
