json.array!(@orders) do |order|
  json.extract! order, :id, :description, :restaurant_id, :order_at, :aasm_state, :handler
  json.url order_url(order, format: :json)
end
