json.array!(@items) do |item|
  json.extract! item, :id, :owner, :name, :price, :number, :is_paid, :order_id, :restaurant_id
  json.url item_url(item, format: :json)
end
