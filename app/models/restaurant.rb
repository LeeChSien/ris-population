class Restaurant < ActiveRecord::Base
  has_many :orders
  has_many :items

  mount_uploader :menu_photo, PhotoUploader

  validates :name, :menu_photo, presence: true
end
