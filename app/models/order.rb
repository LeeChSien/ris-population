class Order < ActiveRecord::Base
  belongs_to :restaurant

  has_many :items

  validates :description, :order_at, :restaurant_id, presence: true

  include AASM

  aasm do
    state :draft, :initial => true
    state :active
    state :archive

    event :open do
      transitions :to => :active
    end

    event :close do
      transitions :from => :active, :to => :archive
    end
  end

  def state_humanize
    name_mapping = {
      'draft'   => '草稿',
      'active'  => '上架中',
      'archive' => '已下架',
    }

    name_mapping[aasm_state]
  end

  def title
    "#{order_at.strftime('%m/%d %a')} #{description}"
  end
end
