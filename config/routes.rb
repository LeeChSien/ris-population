Rails.application.routes.draw do
  resources :orders do
    member do
      post 'open'
      post 'close'
    end
  end

  resources :items
  resources :restaurants

  root 'population_process#index'
  get  'admin' => 'admin#index', as: :admin

  post   'add_item'             => 'main#add_item',    as: :add_item
  delete 'delete_item/:item_id' => 'main#delete_item', as: :delete_item
  get    'typehead'             => 'main#typehead',    as: :typehead

  post 'user/login'  => 'user#login',    as: :user_login
  post 'user/logout' => 'user#logout',   as: :user_logout


  get  'population'         => 'population_process#index',   as: :population_process_index
  post 'population/root'    => 'population_process#root',    as: :population_process_root
  post 'population/general' => 'population_process#general', as: :population_process_general
  post 'population/age_range'      => 'population_process#age_range',      as: :population_process_age_range
  post 'population/root_couple'    => 'population_process#root_couple',    as: :population_process_root_couple
  post 'population/general_couple' => 'population_process#general_couple', as: :population_process_general_couple

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)



  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
