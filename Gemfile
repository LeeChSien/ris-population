source 'https://rubygems.org'
source 'https://rails-assets.org'

gem 'thin'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

# SimpleForm aims to be as flexible as possible while helping you with powerful components to create your forms.
gem 'simple_form'

# This gem provides a simple and extremely flexible way to upload files from Ruby applications.
gem 'carrierwave'
gem 'cloudinary'

# This package contains AASM, a library for adding finite state machines to Ruby classes.
gem 'aasm'

# Do some browser detection with Ruby. Includes ActionController integration.
gem 'browser'

# Generates javascript file that defines all Rails named routes as javascript helpers
gem "js-routes"

# RGL is a framework for graph data structures and algorithms.
gem "rgl"

# Bootstrap-generators provides Twitter Bootstrap generators for Rails 4 (supported Rails >= 3.1).
gem 'bootstrap-generators', git: 'git://github.com/decioferreira/bootstrap-generators.git'

gem 'groupdate'

gem 'rubyzip', '>= 1.0.0' # will load new rubyzip version
gem 'zip-zip' # will load compatibility for old rubyzip API.

########

#-- Rails Assets is the frictionless proxy between Bundler and Bower.
gem 'rails-assets-angular', '1.2.16'
gem 'rails-assets-angular-bootstrap'
gem 'rails-assets-angular-animate'
gem 'rails-assets-angular-validator'
gem 'rails-assets-angular-moment'
gem 'rails-assets-jquery-zoom'
gem 'rails-assets-imagesloaded'
gem 'rails-assets-angular-prompt'
