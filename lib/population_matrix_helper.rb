require 'rgl/base'
require 'rgl/adjacency'
require 'rgl/connected_components'

module PopulationMatrixHelper
  # Instance variable
  # @debug_mode: debug printer
  # @row_length: row length
  # @fix_first_column: boolean

  def print_matrix(matrix)

    checksum_y_arr = []
    matrix[0].size.times do |m|
      checksum_y_arr.push(0)
    end

    matrix.each do |row|
      checksum_x = 0
      row.each_with_index do |item, x|
        if item[:mark] == 1
          checksum_x += 1
          checksum_y_arr[x] += 1
        end
      end

      puts (row.map { |item| item[:mark] } + [checksum_x]).join(',')
    end

    puts checksum_y_arr.join(',')
  end

  def print_matrix_value(matrix)

    checksum_y_arr = []
    matrix[0].size.times do |m|
      checksum_y_arr.push(0)
    end

    matrix.each do |row|
      checksum_x = 0
      row.each_with_index do |item, x|
        if item[:value] == 1
          checksum_x += 1
          checksum_y_arr[x] += 1
        end
      end

      puts (row.map { |item| item[:value] } + [checksum_x]).join(',')
    end
  end


  def print_matrix_m(matrix)
    checksum_y_arr = []
    matrix[0].size.times do |m|
      checksum_y_arr.push(0)
    end

    matrix.each_with_index do |row, y|
      checksum_x = 0
      row.each_with_index do |item, x|
        checksum_x += ((y==0) ? -item[:step] : item[:step])
        checksum_y_arr[x] += ((y==0) ? -item[:step] : item[:step])
      end

      puts (row.map { |item|
        item[:step] > 0 ? "+#{item[:step].round(2)}" : ((item[:step] == 0) ? " #{item[:step].round(2)}" : item[:step].round(2))
      } + [(
          (checksum_x > 0) ? "+#{checksum_x.round(2)}" : ((checksum_x == 0) ? " #{checksum_x.round(2)}" : checksum_x.round(2))
        )]).join(',')
    end

    puts checksum_y_arr.map{ |v|
        v > 0 ? "+#{v.round(2)}" : ((v == 0) ? " #{v.round(2)}" : v.round(2))
      }.join(',')
  end

  def balance_first_column_with_gender!(counties, selected_counties, county_people_male, county_people_female, ignore_first_elem=false)

    problem_matrix_male = []
    problem_matrix_female = []

    selected_counties.each do |county|
      y = counties.index(county)

      row = county_people_male[y]
      m_row = []

      row.each_with_index do |item, x|
        m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
      end

      problem_matrix_male.push m_row

      ##

      row = county_people_female[y]
      m_row = []

      row.each_with_index do |item, x|
        m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
      end

      problem_matrix_female.push m_row
    end

    toggle = true

    if ignore_first_elem
      problem_matrix_male[0][0][:step] = -problem_matrix_male[0][0][:step]
      problem_matrix_female[0][0][:step] = -problem_matrix_female[0][0][:step]

      if problem_matrix_male[0][0][:step] > 0
        toggle = !toggle
      elsif problem_matrix_male[0][0][:step] == 0 and problem_matrix_female[0][0][:step] < 0
        toggle = !toggle
      end
    end

    for y in (ignore_first_elem ? 1 : 0)..problem_matrix_male.size-1
      if (problem_matrix_male[y][0][:value] != problem_matrix_male[y][0][:value].floor) and
         (problem_matrix_female[y][0][:value] != problem_matrix_female[y][0][:value].floor)
         step = toggle ? 0.5 : -0.5

         problem_matrix_male[y][0][:value] += -step
         problem_matrix_female[y][0][:value] += step

         problem_matrix_male[y][0][:step] = step
         problem_matrix_female[y][0][:step] = -step

         problem_matrix_male[y][0][:mark] = problem_matrix_female[y][0][:mark] = 1

         toggle = !toggle
      end
    end

    toggle_male = toggle
    toggle_female = !toggle

    for y in (ignore_first_elem ? 1 : 0)..problem_matrix_male.size-1
      if (problem_matrix_male[y][0][:value] != problem_matrix_male[y][0][:value].floor)
        step = toggle_male ? 0.5 : -0.5
        problem_matrix_male[y][0][:value] += -step
        problem_matrix_male[y][0][:step] = step
        problem_matrix_male[y][0][:mark] = 1
        toggle_male = !toggle_male
      end

      if (problem_matrix_female[y][0][:value] != problem_matrix_female[y][0][:value].floor)
        step = toggle_female ? 0.5 : -0.5
        problem_matrix_female[y][0][:value] += -step
        problem_matrix_female[y][0][:step] = step
        problem_matrix_female[y][0][:mark] = 1
        toggle_female = !toggle_female
      end
    end

    problem_matrix_male[0][0][:step] = -problem_matrix_male[0][0][:step]
    problem_matrix_female[0][0][:step] = -problem_matrix_female[0][0][:step]

    problem_matrix_male.each do |row|
      row.each do |item|
        if item[:mark] == 1
          county_people_male[item[:y]][item[:x]][:step] = item[:step]
          county_people_male[item[:y]][item[:x]][:value] = item[:value]
        end
      end
    end

    problem_matrix_female.each do |row|
      row.each do |item|
        if item[:mark] == 1
          county_people_female[item[:y]][item[:x]][:step] = item[:step]
          county_people_female[item[:y]][item[:x]][:value] = item[:value]
        end
      end
    end

    if ignore_first_elem
      print_matrix_m problem_matrix_male
      puts '---'
      print_matrix_m problem_matrix_female
    end
  end

  def balance_first_row_with_gender!(counties, selected_counties, county_people_male, county_people_female)
    base_y = counties.index(selected_counties.first)

    problem_matrix_male = []
    problem_matrix_female = []

    selected_counties.each do |county|
      y = counties.index(county)

      row = county_people_male[y]
      m_row = []

      row.each_with_index do |item, x|
        m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
      end

      problem_matrix_male.push m_row

      ##

      row = county_people_female[y]
      m_row = []

      row.each_with_index do |item, x|
        m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
      end

      problem_matrix_female.push m_row
    end

    toggle = true

    if problem_matrix_male[0][0][:step] > 0
      toggle = !toggle
    end

    if problem_matrix_male[0][0][:step] == 0 and problem_matrix_female[0][0][:step] < 0
      toggle = !toggle
    end

    for x in 1..problem_matrix_male[base_y].size-1
      if (problem_matrix_male[base_y][x][:value] != problem_matrix_male[base_y][x][:value].floor) and
         (problem_matrix_female[base_y][x][:value] != problem_matrix_female[base_y][x][:value].floor)
         step = toggle ? 0.5 : -0.5

         problem_matrix_male[base_y][x][:value] += step
         problem_matrix_female[base_y][x][:value] += -step

         problem_matrix_male[base_y][x][:step] = step
         problem_matrix_female[base_y][x][:step] = -step

         problem_matrix_male[base_y][x][:mark] = problem_matrix_female[base_y][x][:mark] = 1

         toggle = !toggle
      end
    end

    toggle_male = toggle
    toggle_female = !toggle

    for x in 1..problem_matrix_male[base_y].size-1
      if (problem_matrix_male[base_y][x][:value] != problem_matrix_male[base_y][x][:value].floor)
        step = toggle_male ? 0.5 : -0.5
        problem_matrix_male[base_y][x][:value] += step
        problem_matrix_male[base_y][x][:step] = step
        problem_matrix_male[base_y][x][:mark] = 1
        toggle_male = !toggle_male
      end

      if (problem_matrix_female[base_y][x][:value] != problem_matrix_female[base_y][x][:value].floor)
        step = toggle_female ? 0.5 : -0.5
        problem_matrix_female[base_y][x][:value] += step
        problem_matrix_female[base_y][x][:step] = step
        problem_matrix_female[base_y][x][:mark] = 1
        toggle_female = !toggle_female
      end
    end

    problem_matrix_male.each do |row|
      row.each do |item|
        if item[:mark] == 1
          county_people_male[item[:y]][item[:x]][:step] = item[:step]
          county_people_male[item[:y]][item[:x]][:value] = item[:value]
        end
      end
    end

    problem_matrix_female.each do |row|
      row.each do |item|
        if item[:mark] == 1
          county_people_female[item[:y]][item[:x]][:step] = item[:step]
          county_people_female[item[:y]][item[:x]][:value] = item[:value]
        end
      end
    end

    #print_matrix_m problem_matrix_male
    #puts '---'
    #print_matrix_m problem_matrix_female
  end

  def connectivity_check?(matrix)
    m_height = matrix.size
    m_width  = matrix[0].size

    g = RGL::DirectedAdjacencyGraph[]
    edge_arr = []

    for ext_x in (@fix_first_column ? 1 : 0)..m_width-1 do
      for base_y in 1..m_height-1 do
        swap_target = -matrix[base_y][ext_x][:step]

        if swap_target != 0

          if (matrix[base_y][ext_x][:step] == -swap_target)
            for x in (@fix_first_column ? 1 : 0)..m_width-1 do
              if x == ext_x
                next
              end

              if matrix[base_y][x][:step] == swap_target
                edge_arr.push([(x > ext_x) ? x : ext_x, (x < ext_x) ? x : ext_x])
              end
            end
          end

        end

      end
    end

    edge_arr.uniq!
    edge_arr.each do |e|
      # puts e.join('-')
      g.add_edge(e[0], e[1])
    end

    components = []

    g.to_undirected.each_connected_component { |c| components <<  c }

    if @debug_mode
      p components
    end

    (components.size == 1) ? true : false
  end

  def compute_strategies(matrix)
    # count every columns swap choice
    choices = []
    ref_diff_sum_hash = {}

    m_height = matrix.size
    m_width  = matrix[0].size

    for base_x in (@fix_first_column ? 1 : 0)..m_width-1 do
      # Get head row's step
      diff_sum = -matrix[0][base_x][:step]

      for base_y in 1..m_height-1 do
        step = matrix[base_y][base_x][:step]
        diff_sum += step
      end

      ref_diff_sum_hash[base_x.to_s.to_sym] = diff_sum
    end

    for base_x in (@fix_first_column ? 1 : 0)..m_width-1 do
      # Get head row's step
      diff_sum = -matrix[0][base_x][:step]

      for base_y in 1..m_height-1 do
        step = matrix[base_y][base_x][:step]
        diff_sum += step
      end

      possible_swap_choices = []
      # possible swap choice
      if diff_sum != 0

        for base_y in 1..m_height-1 do
          if (diff_sum > 0.5 or diff_sum < -0.5)
            swap_target = (diff_sum > 0.5) ? -0.5 : 0.5

            if (matrix[base_y][base_x][:step] == -swap_target)
              for x in (@fix_first_column ? 1 : 0)..m_width-1 do
                if x == base_x
                  next
                end

                if matrix[base_y][x][:step] == swap_target and
                    ((ref_diff_sum_hash[x.to_s.to_sym] > 0 and diff_sum < 0) or (ref_diff_sum_hash[x.to_s.to_sym] < 0 and diff_sum > 0))

                  possible_swap_choices.push({
                      target_x: x,
                      target_y: base_y
                    })
                end
              end
            end

          end
        end

        if possible_swap_choices.size == 0
          connectivity = connectivity_check?(matrix)

          if connectivity

            for base_y in 1..m_height-1 do
              if (diff_sum > 0.5 or diff_sum < -0.5)
                swap_target = (diff_sum > 0.5) ? -0.5 : 0.5

                if (matrix[base_y][base_x][:step] == -swap_target)
                  for x in (@fix_first_column ? 1 : 0)..m_width-1 do
                    if x == base_x
                      next
                    end

                    if matrix[base_y][x][:step] == swap_target

                      possible_swap_choices.push({
                          target_x: x,
                          target_y: base_y
                        })
                    end
                  end
                end
              end
            end

          end

        end
      end

      # puts "#{diff_sum}: #{possible_swap_choices.size}"


      choices.push({
          base_x: base_x,
          diff_sum: diff_sum,
          possible_swap_choices: possible_swap_choices
        })
    end

    # Delete if diff_sum == 0
    choices.delete_if { |choice| choice[:diff_sum] == 0 }

    # Sort
    choices.sort_by! do |choice|
      [choice[:possible_swap_choices].size, -choice[:diff_sum].abs]
    end

    if @debug_mode
      choices.each do |choice|
        puts "Candidate column: #{choice[:base_x]}, diff: #{choice[:diff_sum]}, choices: #{choice[:possible_swap_choices].size}"
      end
    end

    return choices
  end

  def check_if_deadlock(choices)
    choices.each do |choice|
      if choice[:possible_swap_choices].size == 0
        return true
      end
    end

    return false
  end

  def swap_choice(matrix, choice)
    swap_index = Random.new.rand(0..choice[:possible_swap_choices].size-1)

    base_x   = choice[:base_x]
    target_x = choice[:possible_swap_choices][swap_index][:target_x]
    target_y = choice[:possible_swap_choices][swap_index][:target_y]

    if @debug_mode
      puts "(#{target_y}, #{base_x}) <=> (#{target_y}, #{target_x})"
    end

    temp_step = matrix[target_y][base_x][:step]
    matrix[target_y][base_x][:step] =  matrix[target_y][target_x][:step]
    matrix[target_y][target_x][:step] =  temp_step
  end

  def write_back!(matrix, county_people)
    matrix.each do |row|
      row.each do |item|
        if item[:mark] == 1
          county_people[item[:y]][item[:x]][:step] = item[:step]
          county_people[item[:y]][item[:x]][:value] += (item[:x] == 0) ? -county_people[item[:y]][item[:x]][:step] : county_people[item[:y]][item[:x]][:step]
        end
      end
    end
  end

  def humanity_fill_step!(matrix)
    toggle = false
    matrix.each do |row|
      if @fix_first_column and row[0][:step] != 0
        toggle = (row[0][:step] == 0.5) ? false : true
      end

      row.each do |item|
        if item[:mark] == 1
          item[:step] = toggle ? 0.5 : -0.5
          toggle = !toggle
        end
      end
    end

    if @debug_mode
      print_matrix_m(matrix)
      puts '~~~~'
    end

    choices = []
    rounds = 500
    while !(choices = compute_strategies(matrix)).empty? and rounds != 0 do
      if check_if_deadlock(choices)
        break
      end

      swap_choice(matrix, choices.first)

      if @debug_mode
        puts '----'
        print_matrix_m(matrix)
        puts '~~~~'
      end

      rounds -= 1
    end

    if choices.empty?
      done = true
      puts 'DONE'
      # print_matrix_m(matrix)
    else
      puts 'FAILED'
      # print_matrix_m(matrix)
      puts '---'
      return false
    end

    matrix.each do |row|
      row.each do |item|
        if item[:step] != 0
          #item[:mark] = 0
        end
      end
    end
    return true
  end

  def solve_matrix(counties, selected_counties, county_people, fix_first_column=false)
    problem_matrix = []

    selected_counties.each do |county|
      y = counties.index(county)

      row = county_people[y]
      m_row = []

      row.each_with_index do |item, x|
        if item[:value] != item[:value].floor
          m_row.push( { mark: 1, step: 0, x: x, y: y, value: item[:value]} )
        else
          m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
        end
      end

      problem_matrix.push m_row
    end


    @fix_first_column = fix_first_column
    if !humanity_fill_step!(problem_matrix)
      return false
    end

    @fix_first_column = false

    write_back!(problem_matrix, county_people)
    return true
  end

end
