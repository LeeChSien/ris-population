require 'csv'

class GeneralPopulationGenderBalanceCalculator
  include PopulationMatrixHelper

  def fill_step_in_row_by_determinate_row!(determinate_row, matrix, y)
    for x in 0..matrix[y].size-1 do
      if matrix[y][x][:value] != matrix[y][x][:value].floor
        temp_value = matrix[y][x][:value]
        matrix[y][x][:value] = determinate_row[x]

        step = temp_value - determinate_row[x]
        matrix[y][x][:step]  = (x==0) ? step : -step # 注意！這裡是相反的，因為這是總數
      end


    end
  end

  def run(source_male_path, source_female_path, result_male_path, result_female_path, row_length = 26)
    @row_length = row_length

    retry_times = 25
    while retry_times != 0 do
      counties = []
      county_people_male = []
      county_people_female = []
      determinate_row_male = []
      determinate_row_female = []
      heading_row = []

      ##

      csv = nil
      # Read file
      csv_text = File.read(source_male_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv_male = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv_male = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv_male.each do |row|
        row_title = row[0] ? row[0].gsub(" ", '') : nil

        case row_count
        when 0
          heading_row = row
        when 1
          determinate_row_male = row[1..@row_length].map { |num_s| num_s.to_f }
        else
          counties.push(row_title)
          county_people_male.push(row[1..@row_length].map { |num| { value: num.to_f, step: 0 } })
        end

        row_count += 1
      end

      ##

      csv = nil
      # Read file
      csv_text = File.read(source_female_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv_female = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv_female = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv_female.each do |row|
        row_title = row[0] ? row[0].gsub(" ", '') : nil

        case row_count
        when 0
          #
        when 1
          determinate_row_female = row[1..@row_length].map { |num_s| num_s.to_f }
        else
          county_people_female.push(row[1..@row_length].map { |num| { value: num.to_f, step: 0 } })
        end

        row_count += 1
      end

      # 依照指定總數調整首行
      fill_step_in_row_by_determinate_row!(determinate_row_male, county_people_male, 0)
      fill_step_in_row_by_determinate_row!(determinate_row_female, county_people_female, 0)

      balance_first_column_with_gender!(counties, counties, county_people_male, county_people_female, true)

      # @debug_mode = true
      # 第一層
      if !solve_matrix(counties, counties, county_people_male, true)
        retry_times -= 1
        next
      end
      if !solve_matrix(counties, counties, county_people_female, true)
        retry_times -= 1
        next
      end
      @debug_mode = false
      puts '1st Level done.'

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people_male[i].map { |item| item[:value] }
        end
      end

      ##

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people_male[i].map { |item| item[:value] }
        end
      end

      # Output
      begin
        File.open(result_male_path, "wb") { |f| f.write(csv_string.encode('big5')) }
      rescue
        File.open(result_male_path, "wb") { |f| f.write(csv_string) }
      end

      ##

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people_female[i].map { |item| item[:value] }
        end
      end

      # Output
      begin
        File.open(result_female_path, "wb") { |f| f.write(csv_string.encode('big5')) }
      rescue
        File.open(result_female_path, "wb") { |f| f.write(csv_string) }
      end

      ##

      break
    end

    if retry_times == 0
      puts "TIMEOUT"
      return false
    else
      puts "COMPLETE"
      return true
    end
  end
end
