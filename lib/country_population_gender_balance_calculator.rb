require 'csv'

class CountryPopulationGenderBalanceCalculator
  include PopulationMatrixHelper

  ALLOW_COUNTIES = [
    "總計",
    "新北市",
    "臺北市",
    "桃園市",
    "臺中市",
    "臺南市",
    "高雄市",
    "臺灣省",
    "宜蘭縣",
    "新竹縣",
    "苗栗縣",
    "彰化縣",
    "南投縣",
    "雲林縣",
    "嘉義縣",
    "屏東縣",
    "臺東縣",
    "花蓮縣",
    "澎湖縣",
    "基隆市",
    "新竹市",
    "嘉義市",
    "福建省",
    "金門縣",
    "連江縣",
  ]

  def run(source_male_path, source_female_path, result_male_path, result_female_path, row_length)
    @row_length = row_length

    retry_times = 25
    while retry_times != 0 do
      counties = []
      county_people_male = []
      county_people_female = []
      heading_row = []

      ##

      csv = nil
      # Read file
      csv_text = File.read(source_male_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv_male = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv_male = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv_male.each do |row|
        if row_count == 0
          heading_row = row
        end

        row_title = row[0] ? row[0].gsub(" ", '') : nil
        if ALLOW_COUNTIES.include?(row_title)
          counties.push(row_title)
          county_people_male.push(row[1..@row_length].map { |num| { value: num.to_f, step: 0 } })
        end
        row_count += 1
      end

      ##

      csv = nil
      # Read file
      csv_text = File.read(source_female_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv_female = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv_female = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv_female.each do |row|
        row_title = row[0] ? row[0].gsub(" ", '') : nil
        if ALLOW_COUNTIES.include?(row_title)
          county_people_female.push(row[1..@row_length].map { |num| { value: num.to_f, step: 0 } })
        end
        row_count += 1
      end

      ##

      selected_counties = [
        "總計",
        "新北市",
        "臺北市",
        "桃園市",
        "臺中市",
        "臺南市",
        "高雄市",
        "臺灣省",
        "福建省",
      ]

      balance_first_column_with_gender!(counties, selected_counties, county_people_male, county_people_female)
      balance_first_row_with_gender!(counties, selected_counties, county_people_male, county_people_female)

      #@debug_mode = true

      # 第一層
      if !solve_matrix(counties, selected_counties, county_people_male, true)
        retry_times -= 1
        next
      end

      if !solve_matrix(counties, selected_counties, county_people_female, true)
        retry_times -= 1
        next
      end
      @debug_mode = false
      puts '1st Level done.'

      # 第二層
      selected_counties2 = [
        '臺灣省',
        "宜蘭縣",
        "新竹縣",
        "苗栗縣",
        "彰化縣",
        "南投縣",
        "雲林縣",
        "嘉義縣",
        "屏東縣",
        "臺東縣",
        "花蓮縣",
        "澎湖縣",
        "基隆市",
        "新竹市",
        "嘉義市",
      ]

      balance_first_column_with_gender!(counties, selected_counties2, county_people_male, county_people_female, true)

      # @debug_mode = true
      if !solve_matrix(counties, selected_counties2, county_people_male, true)
        retry_times -= 1
        next
      end
      if !solve_matrix(counties, selected_counties2, county_people_female, true)
        retry_times -= 1
        next
      end
      puts '2nd Level done. 台灣'

      selected_counties3 = [
        '福建省',
        "金門縣",
        "連江縣",
      ]

      balance_first_column_with_gender!(counties, selected_counties3, county_people_male, county_people_female, true)

      # @debug_mode = true
      if !solve_matrix(counties, selected_counties3, county_people_male, true)
        retry_times -= 1
        next
      end
      if !solve_matrix(counties, selected_counties3, county_people_female, true)
        retry_times -= 1
        next
      end
      puts '2nd Level done. 外島'

      ##

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people_male[i].map { |item| item[:value] }
        end
      end

      # Output
      begin
        File.open(result_male_path, "wb") { |f| f.write(csv_string.encode('big5')) }
      rescue
        File.open(result_male_path, "wb") { |f| f.write(csv_string) }
      end

      ##

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people_female[i].map { |item| item[:value] }
        end
      end

      # Output
      begin
        File.open(result_female_path, "wb") { |f| f.write(csv_string.encode('big5')) }
      rescue
        File.open(result_female_path, "wb") { |f| f.write(csv_string) }
      end

      ##

      break
    end

    if retry_times == 0
      puts "TIMEOUT"
      return false
    else
      puts "COMPLETE"
      return true
    end

  end
end
