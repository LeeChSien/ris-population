class CountryPopulationExpandAgeCalculator < CountryPopulationCalculator
  def write_back!(matrix, county_people, fix_first_column)
    matrix.each do |row|
      row.each do |item|
        if item[:mark] == 1 and (item[:y] and item[:x])
          if fix_first_column
            county_people[item[:y]][item[:x]][:step] = item[:step]
            county_people[item[:y]][item[:x]][:value] += county_people[item[:y]][item[:x]][:step]
          else
            county_people[item[:y]][item[:x]][:step] = (item[:y] == 0) ? -item[:step] : item[:step]
            county_people[item[:y]][item[:x]][:value] += county_people[item[:y]][item[:x]][:step]
          end
        end
      end
    end
  end

  def solve_matrix(counties, selected_counties, county_people, age_range, fix_first_column=false)
    problem_matrix = []
    age_range_matrix = []

    selected_counties.each_with_index do |county, y_index|
      y = counties.index(county)

      row = county_people[y]
      m_row = []
      row.each_with_index do |item, x|
        if item[:value] != item[:value].floor
          m_row.push( { mark: 1, step: 0, x: x, y: y, value: item[:value]} )
        else
          if !fix_first_column
            m_row.push( { mark: 0, step: item[:step], x: x, y: y, value: item[:value]} )
          else
            m_row.push( { mark: 0, step: (y_index == 0) ? -item[:step] : item[:step], x: x, y: y, value: item[:value]} )
          end
        end
      end
      problem_matrix.push m_row

      row = age_range[y]
      a_row = []
      row.each_with_index do |item, x|
        a_row.push(item)
      end
      age_range_matrix.push( a_row )
    end

    for a_x in 0..(age_range_matrix.first.size-1) do
      sub_problem_matrix = []

      p_base_x = (a_x*5)
      p_base_range = 5

      m_row = []
      problem_matrix.each do |row|
        sum = 0
        for p_x in p_base_x..(p_base_x + p_base_range-1) do
          sum += row[p_x][:value]
        end
        m_row.push( { mark: 0, step: row[p_x][:step], x: nil, y: nil, value: sum} )
      end

      for a_y in 0..(age_range_matrix.size-1) do
        temp_value = m_row[a_y][:value]
        m_row[a_y][:value] = age_range_matrix[a_y][a_x][:value]

        step =  age_range_matrix[a_y][a_x][:value] - temp_value
        step = (step == 0) ? 0 : step

        if !fix_first_column
          m_row[a_y][:step] = (a_y==0) ? -step : step # 注意！這裡是相反的，因為這是總數
        elsif a_y != 0
          m_row[a_y][:step] = step # 注意！這裡是相反的，因為這是總數
        end
      end

      sub_problem_matrix.push(m_row)

      for p_x in p_base_x..(p_base_x + p_base_range-1) do
        m_row = []
        for p_y in 0..problem_matrix.size-1
          m_row.push problem_matrix[p_y][p_x]
        end
        sub_problem_matrix.push(m_row)
      end

      @fix_first_column = fix_first_column
      if !humanity_fill_step!(sub_problem_matrix)
        return false
      end
      @fix_first_column = false

      write_back!(sub_problem_matrix, county_people, fix_first_column)
    end
    return true
  end

  def run(source_path, age_range_path, result_path)
    retry_times = 500
    while retry_times != 0 do
      counties = []
      county_people = []
      age_range = []
      heading_row = []

      csv = nil
      # Read file
      csv_text = File.read(source_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv.each do |row|
        if row_count == 0
          heading_row = row
        end

        row_title = row[0] ? row[0].gsub(" ", '').gsub("　", '') : nil
        if ALLOW_COUNTIES.include?(row_title)
          counties.push(row_title)
          county_people.push(row[1..((26-7)*5)].map { |num| { value: num.to_f, step: 0 } })
        end
        row_count += 1
      end


      csv_text = File.read(age_range_path)
      begin
        csv_text_new = csv_text.force_encoding("big5").encode('utf-8')
        csv_age_range = CSV.parse(csv_text_new)
      rescue
        csv_text_new = csv_text.force_encoding('utf-8')
        csv_age_range = CSV.parse(csv_text_new)
      end

      row_count = 0
      csv_age_range.each do |row|
        row_title = row[0] ? row[0].gsub(" ", '') : nil
        if ALLOW_COUNTIES.include?(row_title)
          age_range.push(row[7..25].map { |num| { value: num.to_f, step: 0 } })
        end
        row_count += 1
      end

      selected_counties = [
        "總計",
        "新北市",
        "臺北市",
        "桃園市",
        "臺中市",
        "臺南市",
        "高雄市",
        "臺灣省",
        "福建省",
      ]

      # @debug_mode = true
      # 第一層
      if !solve_matrix(counties, selected_counties, county_people, age_range)
        retry_times -= 1
        next
      end
      @debug_mode = false
      puts '1st Level done.'


      # 第二層
      selected_counties2 = [
        "臺灣省",
        "宜蘭縣",
        "新竹縣",
        "苗栗縣",
        "彰化縣",
        "南投縣",
        "雲林縣",
        "嘉義縣",
        "屏東縣",
        "臺東縣",
        "花蓮縣",
        "澎湖縣",
        "基隆市",
        "新竹市",
        "嘉義市",
      ]

      # @debug_mode = true
      if !solve_matrix(counties, selected_counties2, county_people, age_range, true)
        retry_times -= 1
        next
      end
      puts '2nd Level done. 台灣'

      selected_counties3 = [
        "福建省",
        "金門縣",
        "連江縣",
      ]
      if !solve_matrix(counties, selected_counties3, county_people, age_range, true)
        retry_times -= 1
        next
      end
      puts '2nd Level done. 外島'

      csv_string = CSV.generate do |csv|
        csv << heading_row
        csv << []
        counties.each_with_index do |county, i|
          csv << [county] + county_people[i].map { |item| item[:value] }
        end
      end

      # Output
      begin
        File.open(result_path, "wb") { |f| f.write(csv_string.encode('big5')) }
      rescue
        File.open(result_path, "wb") { |f| f.write(csv_string) }
      end

      break
    end

    if retry_times == 0
      puts "TIMEOUT"
      return false
    else
      puts "COMPLETE"
      return true
    end

  end
end
